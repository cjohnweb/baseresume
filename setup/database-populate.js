var mysql = require('../plugins/mysql'); // mysql connections
var Promise = require('bluebird');
// const { resolve } = require('bluebird');

// Create tables first
let createTables = [
  {
    // SELECT user_id,user_user,user_email,user_perms, user_notifications,user_emailprefs
    name: "Create Users Table",
    args: [],
    query: `CREATE TABLE users (
      id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
      email varchar(255) NOT NULL,
      passwd varchar(255) NOT NULL,
      salt varchar(12) NOT NULL,
      organizations json DEFAULT NULL,
      deviceGroups json DEFAULT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;`
  }
]

let insertData = [
  {
    name: "Adding John's Default User Account",
    query: `INSERT INTO users (id, email, passwd, salt, organizations, deviceGroups) VALUES (?,?,?,?,?,?);`,
    args: [1, "cjohnweb@gmail.com", "pass", "salt", JSON.stringify({ "id": ["1"] }), JSON.stringify({ "id": ["1"] })]
  }
];

async function run(){
  console.log('-- START RUNNING --\n\n');


  console.log('-- Doing Create Tables!\n');
  await createTables.forEach((sql) => {
    doQuery(sql.name, sql.query, sql.args);
  })
  console.log('\n-- DONE Create Tables!\n');

  console.log('-- Doing Inserts!\n');
  await insertData.forEach((sql) => {
    doQuery(sql.name, sql.query, sql.args);
  })
  console.log('\n-- DONE Inserts!\n');
  
  console.log('\n\n-- DONE RUNNING --\n');

}

// run all our queries
run();


function doQuery(tableName, query, values) {
  if (tableName != null) {
    console.log("Executing: ", tableName);
  }
  mysql.getConnection((error, connection) => {
    Promise.fromCallback(function (cb) {
      connection.query(query, [...values], cb)
    }, { multiArgs: true })
      .catch(e => {
        // 'cause', 'isOperational', 'code', 'errno', 'sqlMessage', 'sqlState', 'index', 'sql'
        console.error("MySQL ERROR: ", e.errno, ": ", e.sqlMessage);
      })
      .finally(() => connection.release())
  });
}
