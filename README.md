## MintonTech Base

### Table of Contents

- [Purpose](#purpose)
- [Requirements](#requirements)
- [Installation](#installation)
- [Setup the .env file](#setup-the-env-file)
- [Running](#running)

## Purpose

This repo will spin up a working development environment for the domain name `heartbeatiot.com` with SSL certificates. If you are not operating this at the proper domain name, SSL certs will not generate and you will not be able to access anything. I want to modify this in the future for development and production setups, with certificates, and utilize scripting to clone the appropriate repos and estabish the `.env` files.

## Requirements
- `docker`
- `docker-compose` - runs MySQL and PHPMyAdmin.

## Installation
- Install docker
- Install docker-compose **(but DO NOT RUN THE COMPOSE FILE UNTIL AFTER MODIFYING .ENV FILE)**

## Setup the .env file

Create a new file in the root of your project directory, `.env` and populate it with the following variables. The first time you run `docker-compose up` the password can not be changed unless you delete the volume and recreate it. So make sure to modify these values first before running docker-compose. (This example can be found in from server.env. Copy it's content to .env and modify):

```
NODE_ENV=development
NGINX_PORT=80
NGINX_PORT=443
HEARTBEATIOT_PORT=8080
WORKORDERUP_PORT=8081
MEASURZ_PORT=8082
AUTOMATION_PORT=8083
MYSQL_HOST=mysql
MYSQL_PORT=3306
MYSQL_ROOT_USER=root
MYSQL_ROOT_PASSWORD=<somepassword>
MYSQL_USER=root
MYSQL_PASS=<somepassword>
HEARTBEATIOT_MYSQL_DATABASE=heartbeatiot
WORKORDERUP_MYSQL_DATABASE=workorderup
MEASURZ_MYSQL_DATABASE=measurz
AUTOMATION_MYSQL_DATABASE=automation
MYSQL_ALLOW_EMPTY_PASSWORD=
MYSQL_DEBUG=
PHPMYADMIN_HTTP_PORT=5050
TIMEZONE=America/Los_Angeles
RUNNER_NAME=RUNNER-NAME
REGISTRATION_TOKEN=TOKEN
CI_SERVER_URL=https://gitlab.com/
```

Leaving a variable assigned to nothing will result in a boolean false.

## Docker Compose

The `docker-compose.yml` file contains specifications for Nginx, Let's Encrypt Certbot, Node (x4), MySQL and PHPMyAdmin containers. These containers will use settings from the `.env` file to run. Note that the very first time you run the `docker-compose.yml` file, the password specified in the `.env` file for `MYSQL_ROOT_PASSWORD` will become permanent for the mysql volume in the `docker-compose.yml` file! To "change" the password requires that you export the old database volume to a .sql file, kill docker-compose, delete the old volume/mysql data files, change your password in the .env file and start docker-compose again. This will recreate a new volume with the appropriate name and password, you'll just have to go in and import the database (the SQL file(s) you exported) before moving forward. Point is, **don't run `docker-compose` before you've setup your `.env` file and you are certain the values are all correct**.

Verify that the volume is named appropriately. In this case I opted for mounting the "volumes" into local folders for easy access. See the `./mysql/data/` folder.

Clone the repos `automation`, `workorderup` and `heartbeatiot` into the `./node` folder. Don't worry about running npm install or any of that, this will happen automatically when the Node containers start. If you fail to clone one or more of these repos, nginx or node containers might crash. Just be aware that the node containers expect these folders to contain node projects and that nginx expects those services to be running on these ports for the reverse proxy to work. If a service is not running you may see a "Bad Gateway" error in the browser.

The `.env` file in the root folder of `base` is used by docker-compose. But be aware that each node project also often requires its own `.env` file as well. If the port numbers in these files do not match what nginx expects (port 80 in each container) or if the mysql credentials don't match, you may have a bad time getting things running. I wish there was a way to have a single `.env` file propagate these variables but that's something we'll have to look into later.

The node containers are configured to start the node projects by running a bash file `compose-npm-run-auto.bash` in the root of each node project. If you dont see this file in a repo, a copy of this script exists in the root directory of `base` that you can copy in. Without this file, node will not know how to start your project. By convention, every one of my node projects `package.json` file contains an `auto` script that runs the server in development mode.

If you want to take a look at the `docker-compose.yml` files' node containers, you'll see the line `command: bash compose-npm-run-auto.bash true`. The `true` parameter tells the `compose-npm-run-auto.bash` file to `npm install` when the container first starts. After that, the bash file runs `npm run auto` to start the server in dev mode, watching and rebundling when files change as you work on things.

You should now be able to start Nginx, Node (x4), Certbot, PHPMyAdmin and MySQL by running `docker-compose up`.

You can run in daemon mode (background) with `docker-compose up -d`.

Certbot will continue to update SSL Certificates that already exist. But if you are generating certificates for the first time, there are additional steps that are required. Will have to document this further another time.

## Running

After we have `docker-compose` up and running you should be able to pull up port 80 in the browser which will forward to https port 443 automatically.

Now , we can view the program in the browser at [http://localhost](http://localhost). Note that if we set `HTTP_PORT` to something other than `80` in the `.env` file, say we use `1337`, we will have to use that port number like so: [http://localhost:1337](http://localhost:1337) to access the server on localhost.


# Server Setup

PHPMyAdmin requires HTTPS to access it in the browser. This requires an NGINX Reverse Proxy configured with Let's Encrypt Certificates. Here is a copy of the NGINX reverse proxy for the website as well as PHPMyAdmin.

***cat /etc/nginx/sites-available/heartbeatiot.com***

```
server {
    server_name heartbeatiot.com;

    location / {
        proxy_pass http://127.0.0.1:1337;
        proxy_ssl_trusted_certificate /etc/nginx/ssl/unifi.cer;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/heartbeatiot.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/heartbeatiot.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

server {
    if ($host = heartbeatiot.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    listen [::]:80;
    server_name heartbeatiot.com;
    return 404; # managed by Certbot
}

server {
    server_name heartbeatiot.com;

    location / {
      proxy_pass              http://127.0.0.1:8080;
      proxy_redirect          off;
      proxy_set_header        Host $host;
      proxy_set_header        X-Real-IP $remote_addr;
      proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header        X-Forwarded-Proto $scheme;
    }

    listen [::]:5050 ssl ipv6only=on; # managed by Certbot
    listen 5050 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/heartbeatiot.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/heartbeatiot.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
```

## MySQL Connection Issue
MySQL Latest uses `caching_sha2_password` password authentication by default. NodeJS MySQL plugin does not support this. To change current user password to the `mysql_native_password` authentication plugin, you need to login to MySQL command line and run the following command. Note that root is username and password is the password for that user.

`mysql --host=127.0.0.1 --user=root --password`

One you are signed into MySQL Server, Create the heartbeatiot database as well:

`CREATE DATABASE heartbeatiot; USE heartbeatiot;`

Now alter the user account:

`ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$StrangerThings(555);';`

Don't forget to flush privileges:

`FLUSH PRIVILEGES;`

And that's it, logout to MySQL command line:

`quit`

